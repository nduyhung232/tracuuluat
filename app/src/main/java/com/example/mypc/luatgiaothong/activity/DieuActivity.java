package com.example.mypc.luatgiaothong.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.adapter.ListDieuAdapter;
import com.example.mypc.luatgiaothong.databases.Databases;
import com.example.mypc.luatgiaothong.model.Dieu;

import java.util.ArrayList;

public class DieuActivity extends AppCompatActivity {

    Databases databases;

    int currentChuongId;

    EditText editSearch;
    ListView lvDieu;
    ArrayList<Dieu> dsDieu = new ArrayList();
    ListDieuAdapter listDieuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dieu);

        databases = new Databases(DieuActivity.this);
        Intent intent = getIntent();
        currentChuongId = (int) intent.getSerializableExtra("chuongId");

        addControls();
        addEvents();
    }

    private void addControls() {
        editSearch = findViewById(R.id.editSearch4);
        lvDieu = findViewById(R.id.lvDieu);
        dsDieu = databases.getAllDieuInChuong(currentChuongId);

        listDieuAdapter = new ListDieuAdapter(
                DieuActivity.this,
                R.layout.list_item,
                dsDieu
        );
        lvDieu.setAdapter(listDieuAdapter);
    }

    private void addEvents() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dsDieu.clear();
                dsDieu.addAll(databases.searchDieu(editSearch.getText().toString(), currentChuongId));

                listDieuAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lvDieu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Dieu dieu = (Dieu) parent.getItemAtPosition(position);
                Intent intent = new Intent(DieuActivity.this,ContentActivity.class);
                intent.putExtra("dieu",dieu);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

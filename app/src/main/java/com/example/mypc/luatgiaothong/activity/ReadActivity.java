package com.example.mypc.luatgiaothong.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.databases.Databases;
import com.example.mypc.luatgiaothong.model.Chuong;
import com.example.mypc.luatgiaothong.model.Dieu;

import java.util.ArrayList;

public class ReadActivity extends AppCompatActivity {

    Databases databases;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        databases = new Databases(ReadActivity.this);

        linearLayout = findViewById(R.id.linearLayout);
        getContent();
    }

    public void getContent() {
        ArrayList<Chuong> chuongList = databases.getAllChuong();
        for (Chuong chuong : chuongList) {
            TextView textView1 = new TextView(this);
            textView1.setText(chuong.getTenChuong());
            textView1.setTextColor(this.getResources().getColor(R.color.black));
            textView1.setTextSize(20);
            textView1.setTypeface(Typeface.DEFAULT_BOLD);
            linearLayout.addView(textView1);

            ArrayList<Dieu> dieuList = databases.getAllDieuInChuong(chuong.getId());
            for (Dieu dieu : dieuList) {
                TextView textView2 = new TextView(this);
                String str2 = "<br><strong>" + dieu.getTenDieu() + "</strong>";
                textView2.setText(Html.fromHtml(str2));
                textView2.setTextColor(this.getResources().getColor(R.color.black));
                textView2.setTextSize(17);
                linearLayout.addView(textView2);

                TextView textView3 = new TextView(this);
                textView3.setText(dieu.getNoiDung());
                textView3.setTextColor(this.getResources().getColor(R.color.black));
                linearLayout.addView(textView3);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

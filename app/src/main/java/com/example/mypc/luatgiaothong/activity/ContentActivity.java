package com.example.mypc.luatgiaothong.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.databases.Databases;
import com.example.mypc.luatgiaothong.model.Dieu;

public class ContentActivity extends AppCompatActivity {

    Databases databases;

    EditText editSearch;
    TextView txtContent;

    Dieu currentDieu = new Dieu();
    String currentContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        databases = new Databases(ContentActivity.this);

        Intent intent = getIntent();
        currentDieu = (Dieu) intent.getSerializableExtra("dieu");
        currentContent = currentDieu.getNoiDung();

        addControlls();
        addEvents();
    }

    private void addControlls() {
        editSearch = findViewById(R.id.editSearch4);
        txtContent = findViewById(R.id.txtContent);

        txtContent.setText(currentContent);
    }

    private void addEvents() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                txtContent.setText(currentContent);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editSearch.getText().toString().trim().equals("")) {

                } else {
                    String key1 = editSearch.getText().toString().trim();
                    String key2 = editSearch.getText().toString().trim().toLowerCase();
                    String content = currentContent.replace(key1, "<font color='red'>" + key1 + "</font>");
                    content = content.replace(key2, "<font color='red'>" + key2 + "</font>");
                    content = content.replace("\n", "<br>");
                    txtContent.setText(Html.fromHtml(content));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

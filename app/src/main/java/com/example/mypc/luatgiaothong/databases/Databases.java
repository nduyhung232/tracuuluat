package com.example.mypc.luatgiaothong.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.mypc.luatgiaothong.model.Chuong;
import com.example.mypc.luatgiaothong.model.Dieu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by MyPC on 29/03/2018.
 */

public class Databases {

    Context appContext;
    String DB_PATH_SUFFIX = "/databases/";
    SQLiteDatabase db;

    private static final String DATABASE_NAME = "luatgiaothong.db";

    public Databases(Context context) {
        this.appContext = context;
        xuLySaoChepCSDLTuAssetsVaoHeThongMobile();
        this.db = context.openOrCreateDatabase(DATABASE_NAME, context.MODE_PRIVATE, null);
    }

    public ArrayList<Chuong> getAllChuong() {
        ArrayList<Chuong> list = new ArrayList<>();

        String sql = "select * from chuong";
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Chuong chuong = new Chuong(
                    cursor.getInt(0),
                    cursor.getString(1));
            list.add(chuong);
        }

        return list;
    }

    public ArrayList<Dieu> getAllDieu() {
        ArrayList<Dieu> list = new ArrayList<>();
        String sql = "select * from dieu";

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Dieu dieu = new Dieu(
                    cursor.getInt(0),
                    cursor.getString(2),
                    cursor.getString(3)
            );
            list.add(dieu);
        }

        return list;
    }

    public ArrayList<Dieu> getAllDieuInChuong(int idChuong) {
        ArrayList<Dieu> list = new ArrayList<>();
        String sql = "select * from dieu where idChuong = " + idChuong;

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Dieu dieu = new Dieu(
                    cursor.getInt(0),
                    cursor.getString(2),
                    cursor.getString(3)
            );
            list.add(dieu);
        }

        return list;
    }

    public ArrayList<Chuong> searchChuong(String keyWord) {
        ArrayList<Chuong> list = new ArrayList<>();
        String sql = "select * from chuong where tenChuong like '%" + keyWord + "%'";

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Chuong chuong = new Chuong(
                    cursor.getInt(0),
                    cursor.getString(1));
            list.add(chuong);
        }

        return list;
    }

    public ArrayList<Dieu> searchDieu(String keyWord, int idChuong) {
        ArrayList<Dieu> list = new ArrayList<>();
        String sql = "select * from dieu where tenDieu like '%" + keyWord + "%' and idChuong = " + idChuong;

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Dieu dieu = new Dieu(
                    cursor.getInt(0),
                    cursor.getString(2),
                    cursor.getString(3));
            list.add(dieu);
        }

        return list;
    }

    public ArrayList<Dieu> searchNoiDung(String keyWord) {
        ArrayList<Dieu> list = new ArrayList<>();
        String sql = "select * from dieu where noiDung like '%" + keyWord + "%' or tenDieu like '%" + keyWord + "%'";

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Dieu dieu = new Dieu(
                    cursor.getInt(0),
                    cursor.getString(2),
                    cursor.getString(3));
            list.add(dieu);
        }

        return list;
    }

    private void xuLySaoChepCSDLTuAssetsVaoHeThongMobile() {

        File dbFile = appContext.getDatabasePath(DATABASE_NAME);
        if (!dbFile.exists()) {
            try {
                CopyDatabaseFromAsset();
                Toast.makeText(appContext, "Sao chep thanh cong vao CSDL", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void CopyDatabaseFromAsset() throws IOException {

        InputStream myInput = appContext.getAssets().open(DATABASE_NAME);
        String outFileName = layDuongDanLuuTru();
        File f = new File(appContext.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
        if (!f.exists()) {
            f.mkdir();
        }

        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private String layDuongDanLuuTru() {
        return appContext.getApplicationInfo().dataDir + DB_PATH_SUFFIX + DATABASE_NAME;
    }
}

package com.example.mypc.luatgiaothong.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mypc.luatgiaothong.R;

public class SwitchActivity extends AppCompatActivity {

    TextView txtRead;
    TextView txtSearch1;
    TextView txtSearch2;
    Animation uptodown, downtoup, lefttoright;
    LinearLayout lnCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch);

        addControls();
        addEvents();
    }

    private void addControls() {
        txtRead = findViewById(R.id.txtRead);
        txtSearch1 = findViewById(R.id.txtSearch1);
        txtSearch2 = findViewById(R.id.txtSearch2);
        lnCenter = findViewById(R.id.lnCenter);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.start_uptodown);
        downtoup = AnimationUtils.loadAnimation(this, R.anim.start_downtoup);
        lefttoright = AnimationUtils.loadAnimation(this, R.anim.start_lefttoright);

        txtSearch1.setAnimation(uptodown);
        txtSearch2.setAnimation(downtoup);
        txtRead.setAnimation(lefttoright);
    }

    private void addEvents() {
        final Intent intent1 = new Intent(SwitchActivity.this, ReadActivity.class);
        final Intent intent2 = new Intent(SwitchActivity.this, ChuongActivity.class);
        final Intent intent3 = new Intent(SwitchActivity.this, SearchByKeyWorkActivity.class);

        txtRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent1);
            }
        });
        txtSearch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent2);
            }
        });
        txtSearch2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent3);
            }
        });
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_item:
                Intent intent = new Intent(SwitchActivity.this,IntroductionActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

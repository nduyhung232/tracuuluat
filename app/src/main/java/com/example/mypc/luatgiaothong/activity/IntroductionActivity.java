package com.example.mypc.luatgiaothong.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mypc.luatgiaothong.R;

public class IntroductionActivity extends AppCompatActivity {

    TextView txtIntroduction;
    TextView txtCopyright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        txtIntroduction = findViewById(R.id.introduction);
        txtCopyright = findViewById(R.id.copyright);

        txtIntroduction.setText(
                "Phần mềm được chia thành 3 chức năng riêng biệt:\n" +
                "1.\tĐọc luật:\n" +
                "   Chức năng này dùng để đọc toàn bộ nội dung bộ luật.\n" +
                "2.\tTra cứu theo mục:\n" +
                "   Chức năng này dùng để giúp người dùng tra cứu từng mục trong bộ luật từ ngoài vào trong. Tức là người dùng sẽ dựa vào mục đích mình cần mà tìm kiếm từ Chương đến Điều và cuối cùng vào nội dung luật.\n" +
                "3.\tTra cứu theo từ khóa:\n" +
                "   Chức năng này dùng để giúp người dùng tra cứu trực tiếp theo từ khóa mà họ muốn tra. Những chương hay nội dung luật mà có từ khóa giống như từ khóa mà người dùng nhập vào sẽ được hiện lên và đồng thời các từ khóa ấy cũng được đánh dấu bằng cách bôi đỏ.\n");

        txtCopyright.setText("CopyRight: Phan Văn Vũ");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

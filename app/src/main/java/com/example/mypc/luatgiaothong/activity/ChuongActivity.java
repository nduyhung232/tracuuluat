package com.example.mypc.luatgiaothong.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.adapter.ListChuongAdapter;
import com.example.mypc.luatgiaothong.databases.Databases;
import com.example.mypc.luatgiaothong.model.Chuong;

import java.util.ArrayList;

public class ChuongActivity extends AppCompatActivity {

    Databases databases;
    EditText editSearch;

    ListView lvChuong;
    ArrayList<Chuong> dsChuong = new ArrayList();
    ListChuongAdapter listChuongAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuong);

        databases = new Databases(ChuongActivity.this);

        addControls();
        addEvents();
    }

    private void addControls() {
        editSearch = findViewById(R.id.editSearch4);
        lvChuong = findViewById(R.id.lvChuong);
        dsChuong = databases.getAllChuong();

        listChuongAdapter = new ListChuongAdapter(
                ChuongActivity.this,
                R.layout.list_item,
                dsChuong
        );
        lvChuong.setAdapter(listChuongAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void addEvents() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dsChuong.clear();
                dsChuong.addAll(databases.searchChuong(editSearch.getText().toString()));

                listChuongAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lvChuong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Chuong chuong = (Chuong) parent.getItemAtPosition(position);
                Intent intent = new Intent(ChuongActivity.this,DieuActivity.class);
                intent.putExtra("chuongId",chuong.getId());
                startActivity(intent);
            }
        });

    }
}
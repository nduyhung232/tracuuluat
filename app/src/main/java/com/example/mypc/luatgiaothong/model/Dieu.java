package com.example.mypc.luatgiaothong.model;

import java.io.Serializable;

public class Dieu implements Serializable{
   private int id;
   private String tenDieu;
   private String noiDung;

    public Dieu(int id, String tenDieu, String noiDung) {
        this.id = id;
        this.tenDieu = tenDieu;
        this.noiDung = noiDung;
    }

    public Dieu() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenDieu() {
        return tenDieu;
    }

    public void setTenDieu(String tenDieu) {
        this.tenDieu = tenDieu;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }
}

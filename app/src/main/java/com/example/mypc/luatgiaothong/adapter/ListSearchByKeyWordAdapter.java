package com.example.mypc.luatgiaothong.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.model.Chuong;
import com.example.mypc.luatgiaothong.model.Dieu;

import java.util.List;


/**
 * Created by MyPC on 20/04/2018.
 */

public class ListSearchByKeyWordAdapter extends ArrayAdapter {

    Activity context;
    int resource;
    List objects;

    public ListSearchByKeyWordAdapter(@NonNull Activity context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View view = inflater.inflate(this.resource, null);

        Dieu dieu = (Dieu) objects.get(position);

        String content = "<strong>"+dieu.getTenDieu() + "</strong><br>" + dieu.getNoiDung();
        content = content.replace("\n", "<br>");
        TextView name = view.findViewById(R.id.txtName);
        name.setText(Html.fromHtml(content));

        return view;
    }
}

package com.example.mypc.luatgiaothong.model;

import java.io.Serializable;

public class Chuong implements Serializable {
    int id;
    String tenChuong;

    public Chuong() {
    }

    public Chuong(int id, String tenChuong) {
        this.id = id;
        this.tenChuong = tenChuong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenChuong() {
        return tenChuong;
    }

    public void setTenChuong(String tenChuong) {
        this.tenChuong = tenChuong;
    }
}

package com.example.mypc.luatgiaothong.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;

import com.example.mypc.luatgiaothong.R;
import com.example.mypc.luatgiaothong.adapter.ListSearchByKeyWordAdapter;
import com.example.mypc.luatgiaothong.databases.Databases;
import com.example.mypc.luatgiaothong.model.Dieu;

import java.util.ArrayList;

public class SearchByKeyWorkActivity extends AppCompatActivity {

    Databases databases;

    EditText editSearch;

    ListView listView;
    ArrayList<Dieu> dsDieu = new ArrayList();
    ListSearchByKeyWordAdapter listSearchByKeyWordAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_key_work);

        databases = new Databases(SearchByKeyWorkActivity.this);


        addControls();
        addEvents();
    }

    private void addControls() {
        editSearch = findViewById(R.id.editSearch4);
        listView = findViewById(R.id.listView);

        dsDieu = databases.getAllDieu();

        listSearchByKeyWordAdapter = new ListSearchByKeyWordAdapter(
                SearchByKeyWorkActivity.this,
                R.layout.list_item,
                dsDieu
        );
        listView.setAdapter(listSearchByKeyWordAdapter);
    }

    private void addEvents() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editSearch.getText().toString().trim().equals("")) {
                    dsDieu.clear();
                    dsDieu.addAll(databases.getAllDieu());
                    listSearchByKeyWordAdapter.notifyDataSetChanged();
                } else {
                    String key1 = editSearch.getText().toString().trim();
                    String key2 = editSearch.getText().toString().trim().toLowerCase();
                    dsDieu.clear();
                    dsDieu.addAll(databases.searchNoiDung(key1));
                    for (Dieu dieu : dsDieu) {
                        dieu.setTenDieu(dieu.getTenDieu().replace(key1, "<font color='red'>" + key1 + "</font>"));
                        dieu.setNoiDung(dieu.getNoiDung().replace(key1, "<font color='red'>" + key1 + "</font>"));
                    }

                    for (Dieu dieu : dsDieu) {
                        dieu.setTenDieu(dieu.getTenDieu().replace(key2, "<font color='red'>" + key2 + "</font>"));
                        dieu.setNoiDung(dieu.getNoiDung().replace(key2, "<font color='red'>" + key2 + "</font>"));
                    }

                    listSearchByKeyWordAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
